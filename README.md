# OpenML dataset: Retail_Transaction_Dataset

https://www.openml.org/d/46084

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The Retail_Transaction_Dataset.csv provides a comprehensive overview of various retail transactions, capturing customer behavior, product details, and purchase information. It includes data on customer and product identifiers, quantity and price of items purchased, date and time of transaction, payment methods, store locations, product categories, discounts applied, and the total amount of each transaction. This dataset is formatted in CSV, facilitating ease of use for analysis and reporting purposes.

Attribute Description:
1. CustomerID: Unique numeric identifier for customers (e.g., 770728, 151136).
2. ProductID: Alphabetic identifier for products ('A', 'B', 'C').
3. Quantity: Numeric value representing the number of items purchased (e.g., 7, 5).
4. Price: Numeric value indicating the price of a single item in a transaction, in USD (e.g., 78.11, 72.39).
5. TransactionDate: Date and time of the transaction ('MM/DD/YYYY HH:MM').
6. PaymentMethod: Mode of payment used for the transaction ('Cash', 'Debit Card', 'PayPal').
7. StoreLocation: Address of the store where the transaction occurred.
8. ProductCategory: Category of the product purchased ('Home Decor', 'Books', 'Clothing').
9. DiscountApplied(%): Percentage of discount applied to the transaction.
10. TotalAmount: Total amount paid for the transaction, in USD, after discount.

Use Case:
The Retail_Transaction_Dataset.csv is an invaluable resource for retail analysis, providing insights into customer purchasing patterns, product popularity, seasonal trends in sales, and effectiveness of discount strategies. It can be utilized by data analysts and marketing professionals to optimize inventory management, develop targeted promotions, and implement dynamic pricing strategies to enhance customer satisfaction and maximize profitability. Additionally, the dataset supports academic research in consumer behavior and retail analytics.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46084) of an [OpenML dataset](https://www.openml.org/d/46084). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46084/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46084/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46084/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

